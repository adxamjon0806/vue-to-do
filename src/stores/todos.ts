import { defineStore } from 'pinia'
import { ref } from 'vue'

type todoType = {
  id: number
  title: string
  toDo: string
  status: string
}

const isChangingDefault: {
  id: number | undefined
  title: string | undefined
  toDo: string | undefined
  status: string | undefined
} = {
  id: undefined,
  title: undefined,
  toDo: undefined,
  status: undefined
}

const data: todoType[] = localStorage.getItem('vueTodos')
  ? JSON.parse(localStorage.getItem('vueTodos') || '')
  : []

export const useTodosStore = defineStore('todos', () => {
  // indecator of editing or deleting
  const isChanging = ref(isChangingDefault)
  const todos = ref(data)
  // functions (actions) for chaenging store
  function add(title: string, toDo: string): void {
    todos.value.push({ id: todos.value.length + 1, title, toDo, status: 'pending' })
    localStorage.setItem('vueTodos', JSON.stringify(todos.value))
  }
  function deleteTodo(id: number): void {
    todos.value = todos.value.filter((todo) => todo.id !== id)
    isChanging.value = { id: undefined, title: undefined, toDo: undefined, status: undefined }
    localStorage.setItem('vueTodos', JSON.stringify(todos.value))
  }
  function edit(id: number, title: string, toDo: string, status: string): void {
    const indexOfEditing = todos.value.findIndex((todo) => todo.id === id)
    todos.value[indexOfEditing] = { id, title, toDo, status }
    localStorage.setItem('vueTodos', JSON.stringify(todos.value))
  }
  return { todos, isChanging, add, deleteTodo, edit }
})
